const API = 'https://63ee3f005e9f1583bdbf2d38.mockapi.io';
const formBtn = document.querySelector('#sendBtn');
const table = document.querySelector('#heroesTable');
const tBody = document.createElement('tbody');

renderSelectOptions();

async function controller(method, action, body) {
    const URL = `${API}/${action}`;
    const params = {
        method,
        headers: {
            'Content-Type': 'application/json',
        },
    };

    if (body) params.body = JSON.stringify(body);

    try {
        const response = await fetch(URL, params);
        if (!response.ok) {
            console.log(`HTTP error! Status: ${response.status}`);
        }

        const data = await response.json();
        return data;
    } catch (error) {
        console.log(`Error fetching ${URL}`, error);
    }
}

formBtn.addEventListener('click', async () => {
    const heroFind = await getHeroes();

    if (!heroFind) {
        const newHero = await createHeroes();
        if (newHero) renderHeroes(newHero);
    } else {
        alert('A hero with that name already exists!');
    }
});

async function renderSelectOptions() {
    const select = document.querySelector('#heroComics');
    const universes = await getOptions();

    universes.forEach((universe) => {
        const option = document.createElement('option');
        option.setAttribute('value', universe.name);
        option.innerText = universe.name;
        select.append(option);
    });
}

async function getOptions() {
    return await controller('GET', '/universes');
}

async function createHeroes() {
    const name = document.querySelector('#heroName').value;
    const comics = document.querySelector('#heroComics').value;
    const favourite = document.querySelector('#heroFavourite').checked;

    if (name) {
        const body = {
            name,
            comics,
            favourite,
        };

        const newHero = await controller('POST', '/heroes', body);

        return newHero;
    }
}

async function getHeroes() {
    const heroName = document.querySelector('#heroName').value;
    if (validateInput(heroName)) {
        const response = await controller('GET', '/heroes');
        const heroFind = findHeroByName(response, heroName);
        return heroFind;
    }
}

function validateInput(input) {
    if (input.trim()) {
        return true;
    } else {
        alert('You have to enter a name');
        return false;
    }
}

function findHeroByName(heroes, name) {
    const heroFind = heroes.find((hero) => hero.name === name);
    return heroFind;
}

function renderHeroes(hero) {
    const tr = document.createElement('tr');
    const name = document.createElement('td');
    name.innerText = hero.name;

    const comics = document.createElement('td');
    comics.innerText = hero.comics;

    const favouriteTd = document.createElement('td');
    const favouriteLabel = document.createElement('label');
    const favouriteInput = document.createElement('input');
    favouriteInput.id = hero.name;
    favouriteLabel.innerHTML = 'Favourite:';
    favouriteInput.setAttribute('type', 'checkbox');
    favouriteInput.checked = hero.favourite;

    favouriteInput.addEventListener('change', async () => {
        favouriteInput.checked ? (hero.favourite = true) : (hero.favourite = false);
        await controller('PUT', `/heroes/${hero.id}`, hero);
    });

    const trBtn = document.createElement('td');
    const button = document.createElement('button');
    button.innerHTML = 'Delete';

    button.addEventListener('click', async () => {
        tr.remove();
        await controller('DELETE', `/heroes/${hero.id}`);
    });

    trBtn.append(button);
    favouriteLabel.append(favouriteInput);
    favouriteTd.append(favouriteLabel);

    tr.append(name, comics, favouriteTd, trBtn);
    tBody.append(tr);
    table.append(tBody);
}